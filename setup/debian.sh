#!/bin/bash

which apt || exit 1

apt update

##########################
# Instal packages
##########################

apt -y install \
  abook \
  ansible \
  ark \
  asciidoctor \
  avfs \
  bc \
  blueman \
  bluez \
  breeze \
  breeze-icon-theme \
  calcurse \
  ccrypt \
  cmatrix \
  conky \
  cryptsetup \
  debian-handbook \
  dolphin \
  dosbox \
  elinks \
  feh \
  filelight \
  firefox-esr \
  firewalld \
  fonts-dejavu \
  fonts-hack \
  fonts-liberation \
  fonts-roboto \
  fping \
  fvwm \
  fzf \
  gimp \
  git \
  golang \
  graphviz \
  gwenview \
  htop \
  i2pd \
  iftop \
  imagemagick \
  inotify-tools \
  iotop \
  irssi \
  john \
  kate \
  k3b \
  kcalc \
  kcolorchooser \
  keepassxc \
  kiwix \
  kleopatra \
  kmines \
  kmymoney \
  kolourpaint \
  konsole \
  konversation \
  ksnip \
  ktimer \
  kwrite \
  lame \
  libreoffice \
  libreoffice-gtk3 \
  lightdm \
  lightdm-settings \
  lxappearance \
  lxrandr \
  marble \
  marble-qt \
  mate-themes \
  mc \
  mencoder \
  nginx \
  mplayer \
  mumble \
  mumble-server \
  ncal \
  neofetch \
  neomutt \
  netcat \
  nitrogen \
  nload \
  nmap \
  nmapsi4 \
  nm-tray \
  ntfs-3g \
  okular \
  okular-extra-backends \
  onionshare \
  openjdk-17-jre \
  openttd \
  openvpn \
  pasystray \
  pavucontrol \
  p7zip-full \
  par2 \
  pidgin \
  pidgin-plugin-pack \
  pnmixer \
  pulseaudio \
  pwgen \
  pysolfc \
  python3-xdg \
  qbittorrent \
  qbittorrent-nox \
  qrencode \
  qt5ct \
  remmina \
  remmina-plugin-exec \
  remmina-plugin-rdp \
  remmina-plugin-secret \
  remmina-plugin-spice \
  remmina-plugin-vnc \
  rofi \
  ruby-asciidoctor-pdf \
  sc \
  scrot \
  simplescreenrecorder \
  slick-greeter \
  socat \
  sqlite3 \
  sqlitebrowser \
  sshfs \
  steghide \
  steghide-doc \
  stalonetray \
  strace \
  suckless-tools \
  sudo \
  syncthing \
  syncthing-discosrv \
  syncthing-gtk \
  syncthing-relaysrv \
  tcpdump \
  thunderbird \
  tig \
  tigervnc-common \
  tigervnc-standalone-server \
  tigervnc-viewer \
  tmux \
  tor \
  torsocks \
  toxic \
  tree \
  tshark \
  qtox \
  verse \
  vifm \
  vim \
  vlc \
  wavemon \
  weechat \
  weechat-matrix \
  weechat-scripts \
  wireguard \
  wireshark \
  wireshark-qt \
  xfsprogs \
  xinit \
  xorg \
  xpad \
  youtube-dl \
  zenity \
  zim \
  zint \
  zint-qt \
  || exit 1


##########################
# Stop Systemd services
##########################

systemctl enable firewalld
systemctl start  firewalld

systemctl stop    nginx
systemctl disable nginx

systemctl stop    i2pd
systemctl disable i2pd

systemctl stop    tor
systemctl disable tor

systemctl stop    mumble-server
systemctl disable mumble-server

systemctl stop    syncthing-resume 
systemctl stop    syncthing-discosrv
systemctl stop    syncthing-relaysrv

systemctl disable syncthing-resume
systemctl disable syncthing-discosrv
systemctl disable syncthing-relaysrv

systemctl stop    lightdm
systemctl disable lightdm

##########################
# Install only x86_64
##########################

if [[ -n `uname -a | grep x86_64` ]]
then
  apt -y install \
    qemu-system \
    virtinst \
    libvirt-clients \
    libvirt-daemon-system

  systemctl enable libvirtd
  systemctl start  libvirtd
fi

exit


