#!/bin/bash

if [[ ! -f config ]]; then
  echo "Error: no config found." 
  exit 1
fi

if [[ $1 == '--remove' ]]
then
  [[ -f ~/.xinitrc ]] \
    && rm ~/.xinitrc
  [[ -d ~/.config/conky ]] \
    && rm -rf ~/.config/conky
  [[ -d ~/.fvwm ]] \
    && rm -rf ~/.fvwm
  [[ -d ~/.local/share/konsole ]] \
    && rm -rf ~/.local/share/konsole
  exit
fi

mkdir -vp /etc/skel/.config
mkdir -vp /etc/skel/.fvwm
mkdir -vp /etc/skel/.local/share

cp -v  xinitrc /etc/skel/.xinitrc
cp -v  config  /etc/skel/.fvwm/config
cp -Rv images  /etc/skel/.fvwm
cp -Rv conky   /etc/skel/.config/
cp -Rv konsole /etc/skel/.local/share

cp -v autostart.sh /etc/skel/.fvwm/autostart.sh
chmod a+x /etc/skel/.fvwm/autostart.sh

exit

