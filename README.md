# fvwm-configs

Fvwm is a virtual window manager for the X windows system. It was originally a
feeble fork of TWM by Robert Nation in 1993 (fvwm history), and has evolved
into the fantastic, fabulous, famous, flexible, and so on, window manager we
have today.Fvwm is ICCCM-compliant and highly configurable.
