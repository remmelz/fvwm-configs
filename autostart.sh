#!/bin/bash

# Start Randr
randr_saved=~/.config/autostart/lxrandr-autostart.desktop

if [[ -n `xrandr --listmonitors | grep HDMI` ]]; then
  if [[ -f ${randr_saved} ]]; then
    randr_exec=`cat ${randr_saved} | grep Exec | cut -d"'"  -f2`
    ${randr_exec}
  fi
fi

# Set Wallpaper
feh --bg-scale ~/.fvwm/images/background/debian.png

# Start Conky
which conky
if [[ $? == 0 ]]
then
  conky &
fi

# Start Pulseaudio
which pulseaudio
if [[ $? == 0 ]]
then
  pulseaudio
  pnmixer &
fi

# Start Bluetooth
which blueman-applet
if [[ $? == 0 ]]
then
  blueman-applet &
fi

# Start Networktray
which nm-tray
if [[ $? == 0 ]]
then
  nm-tray &
fi

## Start Xpad
#which xpad
#if [[ $? == 0 ]]
#then
#  xpad &
#fi

# Utox
if [[ -d ~/.config/tox ]]
then
  utox &
fi

# Start Syncthing
which syncthing-gtk
if [[ $? == 0 ]]
then
  syncthing-gtk --minimized &
fi

